package org.tinygroup.jdbctemplatedslsession.callback;

import org.tinygroup.tinysqldsl.Delete;

public interface NoParamDeleteGenerateCallback{
	
	public Delete generate();

}
